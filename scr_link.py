#!python3

from pathlib import Path
from os import symlink, chmod
from os import environ as env
from sys import argv
from stat import S_IEXEC

SRC_DIR = Path(env["HOME"]) / Path('devel/scripts')
BIN_DIR = Path(env["HOME"]) / Path('.local/bin')

def make_link(script_name, src_dir=SRC_DIR, bin_dir=BIN_DIR):
    script = src_dir / script_name
    if not script.exists() or script.is_dir():
        print("Script {} does not exist, or is a directory. Exiting...".format(script))
        exit(255)

    #strip file extension from script name for executable name
    bin_name = script_name[:script_name.find('.')]
    bin_target = bin_dir / bin_name
    if bin_target.exists():
        print("Executable {} exists. Exiting...".format(bin_target))
        exit(255)

    symlink(script, bin_target)
    print("Linked {0} to {1}.".format(script, bin_target))


if __name__ == "__main__":
    make_link(argv[1])

