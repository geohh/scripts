#!/bin/dash
helptext()
{
    printf "USAGE: tunnel_reconnect [remotehost] [port]

            Checks if a tunnel is still listening locally to [port],
            if not, runs `ssh -L` to reconnect.
"
}

if [ ! $# -eq 2 ]; then
    helptext
    exit 1
fi

REMOTEHOST=$1
PORT=$2



nc -z localhost $PORT || ssh -NfL ${PORT}:${REMOTEHOST}:${PORT} ${USER}@${REMOTEHOST}


