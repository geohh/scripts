#! /usr/bin/python3

import subprocess, sys, os
from getopt import getopt

DEFAULT_MOUNT_OPTIONS = { \
        "allow_other": None, \
        "defer_permissions": None, \
        "noappledouble": None, \
        "follow_symlinks": None, \
        "IdentityFile": "{}/.ssh/id_rsa".format(os.environ["HOME"]), \
        "idmap": "user", \
        "reconnect": None, \
#        "ServerAliveInterval": 15, \
#        "ServerAliveCountMax": 3, \
        "Compression": "no", \
        "auto_cache": None, \
        "no_remote_lock": None, \
                        }

SSHFS_COMMAND = 'sudo sshfs'
#SSHFS_COMMAND = '/usr/bin/sshfs'
DEFAULT_FLAGS = [ \
#        "-C", \
        ]

TARGETS = { \
        "bravo": ("hutch@bwrcrdsl-4.eecs.berkeley.edu:/tools/projects/ISG/isg_chips/projects/bravo/user/hutch/bravo_top", "{}/devel/bwrc/bravo".format(os.environ["HOME"])), \
        "bwrc": ("hutch@bwrcrdsl-4.eecs.berkeley.edu:", "{}/.local/mnt/bwrc".format(os.environ["HOME"])), \
        }


DEFAULT_REMOTE = "hutch@bwrcrdsl-4.eecs.berkeley.edu:"
DEFAULT_LOCAL  = "bravo"

def mount(flags, options, remote, local):
    option_str = ""
    for opt, val in options.items():
        if val is None:
            option_str += "{},".format(opt)
        else:
            option_str += "{0}={1},".format(opt, val)
    if option_str[-1] == ',': #Drop comma from last option
        option_str = option_str[:-1]

    cmd = SSHFS_COMMAND.split()
    cmd += flags
    cmd += ["-o", option_str]
    cmd.append(remote)
    cmd.append(local)

    print(cmd)

    subprocess.call(cmd, \
#            shell=True, \
            stdin=subprocess.PIPE, \
            stdout=subprocess.PIPE, \
            stderr=subprocess.PIPE, \
            )
#    out, err = proc.communicate()
#    print(out)

def unmount(remote):
    cmd = ['sudo', 'umount', remote]
    subprocess.call(cmd, \
#            shell=True, \
            stdin=subprocess.PIPE, \
            stdout=subprocess.PIPE, \
            stderr=subprocess.PIPE, \
            )


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Syntax: sshmnt.py (mount|unmount) [target_id]")
        sys.exit()

    command = sys.argv[1]
    target_id = sys.argv[2]
    remote, local = TARGETS[target_id]
    if command == "mount":
        print("Attempting to mount {0} at {1}...".format(remote, local))
        mount(DEFAULT_FLAGS, DEFAULT_MOUNT_OPTIONS, remote, local)
    elif command == "unmount":
        print("Attempting to unmount {0} at {1}...".format(remote, local))
        unmount(remote)


