#!/bin/dash


EXEC_PATH=${HOME}/.local/bin

SCRIPT_FILENAME=$1
SCRIPT_EXECUTABLE_NAME=${SCRIPT_FILENAME%.*}
SCRIPT_EXECUTABLE_FULL=${EXEC_PATH}/${SCRIPT_EXECUTABLE_NAME}

helptext()
{
    printf "USAGE: scr_link [script name]\n\nMakes a symlink to target script.\n\n"
}

if [ ! $# -eq 1 ]; then
    helptext
    exit 1
fi


if [ ! -f $SCRIPT_FILENAME ]; then
    printf "Can't find script"
    exit 255
elif [ -f ${SCRIPT_EXECUTABLE_FULL} ]; then
    printf "Executable already exists"
    exit 255
else
    ln -s ${PWD}/${SCRIPT_FILENAME} ${SCRIPT_EXECUTABLE_FULL}
    chmod a+x $SCRIPT_EXECUTABLE_FULL
    chmod a+x ${PWD}/${SCRIPT_FILENAME}
fi


